<?php

namespace Controllers;

use \http\Response;

class LoginController
{
  private $templates;

  public static function __dependencies()
  {
    return ['templates'];
  }

  public function setTemplates($templates)
  {
    $this->templates = $templates;
  }

  public function index($request)
  {
    return new Response($this->templates->find('login.phtml')->render(array()));
  }
}
