<?php

namespace Controllers;

use \http\Response;

class HomeController
{
    public static function __dependencies()
    {
        return [];
    }

    public function index($request)
    {
      $user = $request->getParam('user');

      if (!$user) {
        return Response::redirect('/login');
      } else {
        return Response::redirect('/dashboard');
      }
    }
}
