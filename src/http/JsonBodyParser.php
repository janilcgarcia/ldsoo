<?php

namespace http;

class JsonBodyParser implements BodyParser
{
    public function parse($body)
    {
        return json_decode($body, true);
    }
}
