<?php

namespace http;

class Response
{
    private $status;
    private $body;
    private $headers;
    private $content_type;
    private $cookies;
    private $encoding;

    public function __construct($body = '', $status = 200,
                                $content_type = 'text/html',
                                $encoding = 'utf-8')
    {
        $this->status = $status;
        $this->body = $body;
        $this->headers = array();
        $this->content_type = $content_type;
        $this->encoding = $encoding;
    }

    public function setHeader($id, $value, $append = true)
    {
        $id = strtolower($id);

        if ($id === 'content-type')
        {
            $this->content_type = trim(explode(';', $value)[0]);
            return;
        }

        if (!$append || !array_key_exists($id, $this->headers))
            $this->headers[$id] = array();

        array_push($this->headers[$id], $value);

        return $this;
    }

    public function getHeader($id)
    {
        return $this->headers[strtolower($id)][0];
    }

    public function setStatus($code)
    {
        $this->status = $code;
        return $this;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function getHeaders($id)
    {
        return $this->headers[strtolower($id)];
    }

    public function getAllHeaders()
    {
        return $this->headers;
    }

    public function unsetHeader($id)
    {
        unset($this->headers[strtolower($id)]);
    }

    public function setContentType($ct)
    {
        $this->content_type = $ct;
        return $this;
    }

    public function setBody($body)
    {
        $this->body = $body;
    }

    public function getContentType()
    {
        return $this->content_type;
    }

    public function getBody()
    {
        return $this->body;
    }

    public function getEncoding()
    {
        return $this->encoding;
    }

    public static function redirect($url, $code = 302)
    {
      $r = new Response('', $code);

      $r->setHeader('Location', $url);

      return $r;
    }
}
