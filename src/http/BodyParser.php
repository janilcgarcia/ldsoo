<?php

namespace http;

interface BodyParser
{
    public function parse($body);
}
