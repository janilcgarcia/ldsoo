<?php

namespace http;

class QueryDict
{
    private $dict;
    
    public function __construct()
    {
        $this->dict = [];
    }

    public function set($name, $value)
    {
        if ($value !== TRUE)
            $this->dict[$name] = [$value];
        else
            $this->dict[$name] = [];
    }

    public function append($name, $value)
    {
        if (!isset($this->dict[$name]))
            $this->dict[$name] = [];

        array_push($this->dict[$name], $value);
    }

    public function remove($name)
    {
        unset($this->dict[$name]);
    }

    public function has($name)
    {
        return isset($this->dict[$name]);
    }

    public function getAll($name)
    {
        if (!isset($this->dict[$name]))
            return null;

        return $this->dict[$name];
    }

    public function get($name)
    {
        $all = $this->getAll();
        
        if ($all)
            return $all[0];
        
        return null;
    }
}
