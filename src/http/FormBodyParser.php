<?php

namespace http;

class FormBodyParser implements BodyParser
{
    public function parse($body)
    {
        $query = new QueryDict();
        
        $body = urldecode($body);

        foreach(explode('&', $body) as $chunk)
        {
            $param = explode('=', $chunk);

            if ($param)
            {
                $name = $param[0];
                $value = isset($param[1]) ? $param[1] : TRUE;
                
                $query->append($param[0], $param[1]);
            }
        }

        return $query;
    }
}
