<?php

namespace http;

class Router
{
    private $locator;
    private $routes;

    public static function __dependencies()
    {
        return ['locator'];
    }

    public function __construct()
    {
        $this->routes = [];
    }

    public function setLocator($locator)
    {
        $this->locator = $locator;
    }

    public function register($path, $action, $methods = ['get'])
    {
        $this->routes[] = new Route($path, $action, $methods);
    }

    public function dispatch($request)
    {
        foreach ($this->routes as $route)
        {
            if ($result = $route->match($request))
            {
                $parts = explode('#', $result[0]);
                $controller = $this->locator
                            ->find('controller/' . $parts[0]);

                return $controller->{$parts[1]}($request);
            }
        }

        return null;
    }
}
