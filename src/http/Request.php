<?php

namespace http;

class Request
{
    private $method;
    private $content_type;
    private $headers;

    private $data;

    private $path;

    private $query;

    private $params;

    public function __construct($locator)
    {
        $this->method = strtolower($_SERVER['REQUEST_METHOD']);

        $this->headers = [];

        if (!$this->getHeader('Content-Type'))
            $this->content_type = null;
        else
            $this->content_type = explode(
                ';', $this->getHeader('Content-Type'))[0];

        $this->makeHeaders($_SERVER, '!^HTTP_(.*)$!');

        $this->data = [];

        if (in_array(strtolower($this->method), ['post', 'put', 'patch']))
        {
            $body_parsers = $locator->find('bodyParsers');
            $body_parser = $body_parsers
                         ->forContentType($this->content_type);

            if (!$body_parser)
                return;

            $this->data = $this->body_parser
                        ->parse(file_get_contents('php://input'));
        }

        $this->path = $_SERVER['PATH_INFO'];

        $this->query = new QueryDict();

        $q = urldecode($_SERVER['QUERY_STRING']);

        foreach (explode('&', $q) as $fp) {
          $p = strpos($fp, '=');
          if ($p === FALSE)
            $this->query->append($fp, TRUE);
          else
            $this->query->append(substr($fp, 0, $p), substr($fp, $p));
        }

        $this->params = [];
    }

    public function getMethod()
    {
        return $this->method;
    }

    private function makeHeaders($vars, $regex)
    {
        foreach ($vars as $key => $value)
        {
            if (preg_match($regex, $key, $matches))
            {
                $name = $matches[1];
                $name = strtolower($name);
                $name = str_replace('_', '-', $name);

                $this->headers[$name] = $value;
            }
        }
    }

    public function getContentType()
    {
        return $this->content_type;
    }

    public function getHeader($name)
    {
        $name = strtolower(str_replace('-', '_', $name));

        if (array_key_exists($name, $this->headers))
            return $this->headers[$name];
        else
            return null;
    }

    public function getData()
    {
        if ($this->data)
            return $this->data;

        return file_get_contents('php://input');
    }

    public function getPath()
    {
        return $this->path;
    }

    public function getQuery()
    {
      return $this->query;
    }

    public function getParam($id)
    {
      return isset($this->params[$id]) ? $this->params[$id] : null;
    }

    public function setParam($id, $value)
    {
      $this->params[$id] = $value;

      return $this;
    }
}
