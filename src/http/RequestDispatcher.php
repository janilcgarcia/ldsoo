<?php

namespace http;

class RequestDispatcher
{
    private $router;
    private $locator;

    public static function __dependencies()
    {
        return ['router', 'locator'];
    }

    public function setRouter($router)
    {
        $this->router = $router;
    }

    public function setLocator($locator)
    {
        $this->locator = $locator;
    }

    public function dispatch()
    {
        $request = new Request($this->locator);

        $response = $this->router->dispatch($request);

        if ($response)
        {
            $code = $response->getStatus();
            http_response_code($code);

            $headers = $response->getAllHeaders();

            foreach ($headers as $key => $value)
            {
                header($key . ': ' . implode('; ', $value));
            }

            header('Content-Type: '
                   . $response->getContentType() . '; charset='
                   . $response->getEncoding());

            if ($body = $response->getBody())
                echo $body;
        }
        else
        {
            http_response_code(405);
        }
    }
}
