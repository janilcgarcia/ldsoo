<?php

namespace http;

class Route
{
    private $methods;
    private $path;
    private $action;

    public function __construct($path, $action, $methods)
    {
        $this->path = preg_quote($path, '!');
        $this->path = '!^'
                    . preg_replace('/\\\\{(.+?)\\\\}/', '(?P<\1>.+?)',
                                   $this->path)
                    . '$!';

        
        $this->action = $action;
        $this->methods = $methods;
    }

    public function match($request)
    {
        if (!in_array($request->getMethod(), $this->methods))
            return FALSE;

        if (!preg_match($this->path, $request->getPath(), $matches))
            return FALSE;

        return [$this->action, $matches];
    }
}
