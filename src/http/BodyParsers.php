<?php

class BodyParsers
{
    private $parsers;

    public function __construct()
    {
        $this->parsers = array();
    }

    public function register($content_types, $parser)
    {
        foreach ($content_type as $type)
            $this->parsers[$type] = $parser;
    }

    public function forContentType($ct)
    {
        if (!array_key_exists($ct, $this->parsers))
        {
            return null;
        }

        return $this->parsers[$ct];
    }
}
