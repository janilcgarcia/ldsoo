<?php

namespace Modus;

class TemplateGroup implements TemplateElement
{
  private $elements;

  public function __construct()
  {
    $this->elements = array();
  }

  public function add($element)
  {
    $this->elements[] = $element;
  }

  public function render($data, $sections)
  {
    $s = '';

    foreach ($this->elements as $el)
      $s .= $el->render($data, $sections);

    return $s;
  }

  public function getLength()
  {
    return count($this->elements);
  }

  public function clean()
  {
    $this->elements = array();
  }
}
