<?php

namespace Modus;

class TemplateContext
{
  private $data;
  private $engine;
  private $name;

  private $sections;

  private $collecting;
  private $in_section;

  private $root_group;

  public function __construct($name, $data, $engine)
  {
    $this->data = $data;
    $this->engine = $engine;
    $this->name = $name;
    $this->sections = array();
    $this->in_section = null;

    $this->root_group = new TemplateGroup();
    $this->group = $this->root_group;

    $this->collecting = true;
  }

  protected function extend($parent)
  {
    if ($this->group->getLength() !== 0 || \ob_get_length() !== 0) {
      throw new TemplateException('Extend must be the first statement in the file');
    }

    \ob_clean();

    $parent_ctx = new TemplateContext($parent, $this->data, $this->engine);
    $parent_ctx->process();

    $this->collecting = false;

    $this->root_group = $parent_ctx->group;
    $this->group = $this->root_group;

    $this->sections = $parent_ctx->sections;
  }

  protected function section($name)
  {
    $this->sections[$name] = new TemplateGroup();
    $this->storeOutput();
    $this->group->add(new TemplateSection($name));
  }

  protected function startSection($name)
  {
    if ($this->in_section) {
      throw new TemplateException('A section can\'t be started from the inside'
          . ' another section');
    }

    $this->storeOutput();

    $section = $this->sections[$name];
    $section->clean();

    $this->group = $section;

    $this->in_section = true;
    \ob_start();
  }

  protected function endSection()
  {
    $this->group->add(new TemplateText(ob_get_contents()));
    $this->group = $this->root_group;

    $this->in_section = false;
    \ob_end_clean();
  }

  protected function includeFrom($name)
  {
    $this->storeOutput();

    $ctx = new TemplateContext($name, $this->data, $this->engine);
    $ctx->process();

    $this->group->add($ctx->getGroup());
  }

  public function getGroup()
  {
    return $this->root_group;
  }

  public function getSections()
  {
    return $this->sections;
  }

  public function process()
  {
    $data = $this->data;
    $h = $this->engine->getHelpers();

    \ob_start();
    include $this->engine->findTemplateFile($this->name);

    $this->storeOutput();

    \ob_end_clean();
  }

  private function storeOutput()
  {
    if ($this->collecting || $this->in_section) {
      $this->group->add(new TemplateText(ob_get_contents()));
    }

    ob_clean();
  }
}
