<?php

namespace Modus;

class TemplateText implements TemplateElement
{
  private $text;

  public function __construct($text)
  {
    $this->text = $text;
  }

  public function render($data, $sections)
  {
    return $this->text;
  }
}
