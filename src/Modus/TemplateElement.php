<?php

namespace Modus;

interface TemplateElement
{
  public function render($data, $sections);
}
