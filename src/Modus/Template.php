<?php

namespace Modus;

class Template
{
  private $name;
  private $data;
  private $engine;
  private $subsections;
  private $currentSection;

  public function __construct($name, $engine, $data)
  {
    $this->name = $name;
    $this->data = $data;
    $this->subsections = array();
    $this->engine = $engine;
    $this->currentSection = array();

    $this->group = new TemplateGroup;
  }

  public function getName()
  {
    return $this->name;
  }

  public function render($data)
  {
    $data = array_merge($this->data, $data);

    $ctx = new TemplateContext($this->name, array_merge($this->data, $data),
        $this->engine);

    $ctx->process();

    $group = $ctx->getGroup();
    $sections = $ctx->getSections();

    return $group->render($data, $sections);
  }
}
