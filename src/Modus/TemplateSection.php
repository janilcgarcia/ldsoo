<?php

namespace Modus;

class TemplateSection implements TemplateElement
{
  private $name;

  public function __construct($name)
  {
    $this->name = $name;
  }

  public function render($data, $sections)
  {
    return $sections[$this->name]->render($data, $sections);
  }
}
