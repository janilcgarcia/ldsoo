<?php

namespace Modus;

class Engine
{
  private $path;
  private $helpers;

  public function __construct($path, $data)
  {
    $this->path = realpath($path);
    $this->helpers = array();
    $this->data = array();
  }

  public function findTemplateFile($file)
  {
    return $this->path . '/' . $file;
  }

  public function find($template)
  {
    return new Template($template, $this, $this->data);
  }

  public function getHelpers()
  {
    return $this->helpers;
  }

  public function registerHelper($name, $callback)
  {
    $this->helpers->{$name} = $callback;
  }
}
