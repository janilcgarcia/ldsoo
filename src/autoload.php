<?php

class ModularAutoloader
{
    private $loaded;
    
    public function __construct()
    {
        $this->loaded = array();
    }

    public function load($class_name)
    {
        $parts = explode('\\', $class_name);

        if (count($parts) == 1)
        {
            include $class_name . '.php';
        }
        else
        {
            $module = $parts[0];
            
            if (!in_array($module, $this->loaded))
                $this->loadModule($module);

            $class_path = implode('/', $parts) . '.php';

            include $class_path;
        }
    }

    private function loadModule($module)
    {
        if (file_exists($module . '/module.php'))
            include $module . '/module.php';

        $this->loaded[] = $module;
    }
}

spl_autoload_register([new ModularAutoloader(), 'load']);
