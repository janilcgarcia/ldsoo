<?php

namespace Model;

class EventType extends \Mimir\Model
{
  const fields = array('id', 'name');
  const table = "event_types";
}
