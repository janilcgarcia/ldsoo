<?php

namespace Model;

class VolunteerSchedule extends \Mimir\Model
{
  const fields = array('id', 'event_id', 'volunteer_id', 'activity_id', 'status');
  const table = "volunteer_schedule";
}
