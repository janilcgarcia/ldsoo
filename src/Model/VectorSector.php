<?php

namespace Model;

class VolunteerSector extends \Mimir\Model
{
  const fields = array('volunteer_id', 'sector_id');
  const table = "volunteer_sector";
}
