<?php

namespace Model;

class Activity extends \Mimir\Model
{
  const fields = array('id', 'name', 'description', 'sector_id');
  const table = 'activities';
}
