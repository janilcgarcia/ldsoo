<?php

namespace Model;

class User extends \Mimir\Model
{
  const fields = array('id', 'name', 'email', 'hash', 'admin');
  const table = 'users';

  public function setPassword($password)
  {
    $this->set('hash', \password_hash($password, PASSWORD_DEFAULT));
  }

  public function verifyPassword($password)
  {
    $h = $this->get('hash');

    $r = \password_verify($password, $h);

    if (!$r)
      return false;

    if (\password_needs_rehash($h, PASSWORD_DEFAULT)) {
      $this->set('hash', \password_hash($password, PASSWORD_DEFAULT));
    }

    return $r;
  }
}
