<?php

namespace Model;

class Sector extends \Mimir\Model
{
  const fields = array('id', 'name');
  const table = 'sectors';
}
