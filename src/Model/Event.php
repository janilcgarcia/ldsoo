<?php

namespace Model;

class Event extends \Mimir\Model
{
  const fields = array('id', 'name', 'event_type_id', 'date');
  const table = "events";
}
