<?php

namespace Mimir\Segments;

class IdSegment implements QuerySegment
{
  private $id;

  use Comparable;

  public function __construct($id)
  {
    $this->id = $id;
  }

  public function text()
  {
    return '"' . $this->id . '"';
  }

  public function values()
  {
    return array();
  }
}
