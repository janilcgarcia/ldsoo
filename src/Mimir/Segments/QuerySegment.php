<?php

namespace Mimir\Segments;

interface QuerySegment
{
  public function text();
  public function values();
}
