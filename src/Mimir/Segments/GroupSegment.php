<?php

namespace Mimir\Segments;

class GroupSegment extends CompositeSegment
{
  private $parenthesis;

  public function __construct($parenthesis = false)
  {
    parent::__construct(', ');
    $this->parenthesis = $parenthesis;
  }

  public function text()
  {
    if ($this->parenthesis)
      return '(' . parent::text() . ')';
    else
      return parent::text();
  }
}
