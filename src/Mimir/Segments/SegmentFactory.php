<?php

namespace Mimir\Segments;

class SegmentFactory
{
  private static $instance;

  public static function getInstance()
  {
    if (!self::$instance)
      self::$instance = new self();

    return self::$instance;
  }

  private function __construct() {}
  public function op($a, $op, $b)
  {
    return new BinOpSegment($a, $op, $b);
  }

  public function composite($separator, ...$elements)
  {
    $s = new CompositeSegment($separator);

    foreach ($elements as $el) {
      if ($el instanceof QuerySegment)
        $s->add($el);
      else
        $s->add(new PlainSegment($el));
    }

    return $s;
  }

  public function group($paren, ...$elements)
  {
    $s = new GroupSegment($paren);

    foreach ($elements as $el) {
      if ($el instanceof QuerySegment) {
        $s->add($el);
      } else {
        $s->add(new PlainSegment($el));
      }
    }

    return $s;
  }

  public function plain($text)
  {
    return new PlainSegment($text);
  }

  public function value($value)
  {
    return new ValueSegment($value);
  }

  public function id($id)
  {
    return new IdSegment($id);
  }

  public function and(...$vs)
  {
    if (count($vs) <= 1)
      return $vs[0];

    $a = array_shift($vs);
    $b = array_shift($vs);

    $a = $this->op($a, 'AND', $b);

    while (($b = array_shift($vs))) {
      $a = $this->op($a, 'AND', $b);
    }

    return $a;
  }

  public function or(...$vs)
  {
    if (count($vs) <= 1)
      return $vs[0];

    $a = array_shift($vs);
    $b = array_shift($vs);

    $a = $this->op($a, 'OR', $b);

    while (($b = array_shift($vs))) {
      $a = $this->op($a, 'OR', $b);
    }

    return $a;
  }
}
