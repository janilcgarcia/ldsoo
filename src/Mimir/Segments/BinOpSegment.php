<?php

namespace Mimir\Segments;

class BinOpSegment
{
  private $a, $b;
  private $op;

  const PRECEDENCE = array(
    'OR' => 1,
    'AND' => 2,
    'NOT' => 3,
    '>' => 4, '<' => 4, '>=' => 4, '<=' => 4, '<>' => 4, '=' => 4,
    '+' => 5, '-' => 5, '||' => 5,
    '*' => 6, '/' => 6
  );

  public function __construct($a, $op, $b)
  {
    $this->a = $a;
    $this->b = $b;
    $this->op = $op;
  }

  private function operandText($x)
  {
    if ($x instanceof BinOpSegment &&
        self::PRECEDENCE[$x->op] <= self::PRECEDENCE[$this->op])
      return '(' . $x->text() . ')';
    else
      return $x->text();
  }

  public function text()
  {
    $t = [];

    $t[] = $this->operandText($this->a);
    $t[] = $this->op;
    $t[] = $this->operandText($this->b);

    return implode(' ', $t);
  }

  public function values()
  {
    $v = [];

    $v[] = $this->a->values();
    $v[] = $this->b->values();

    return array_merge(...$v);
  }
}
