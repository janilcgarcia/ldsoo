<?php

namespace Mimir\Segments;

class CompositeSegment implements QuerySegment
{
  private $segments;
  private $separator;

  public function __construct($separator = ' ')
  {
    $this->segments = array();
    $this->separator = $separator;
  }

  public function add($segment)
  {
    $this->segments[] = $segment;
  }

  public function clear()
  {
    $this->segments = [];
  }

  public function text()
  {
    $t = array();

    foreach ($this->segments as $segment) {
      $t[] = $segment->text();
    }

    return implode($this->separator, $t);
  }

  public function values()
  {
    $v = array();

    foreach ($this->segments as $segment) {
      $v[] = $segment->values();
    }

    return array_merge(...$v);
  }
}
