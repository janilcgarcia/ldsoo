<?php

namespace Mimir\Segments;

class PlainSegment implements QuerySegment
{
  private $text;

  public function __construct($text)
  {
    $this->text = $text;
  }

  public function text()
  {
    return $this->text;
  }

  public function values()
  {
    return array();
  }
}
