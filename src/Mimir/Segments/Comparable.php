<?php

namespace Mimir\Segments;

trait Comparable
{
  private function buildOpSegment($a, $op, $b)
  {
    $sf = SegmentFactory::getInstance();

    if (!($a instanceof QuerySegment))
      $a = $sf->value($a);

    if (!($b instanceof QuerySegment))
      $b = $sf->value($b);

    return new BinOpSegment($a, $op, $b);
  }

  public function eq($other)
  {
    return $this->buildOpSegment($this, '=', $other);
  }

  public function neq($other)
  {
    return $this->buildOpSegment($this, '<>', $other);
  }

  public function gt($other)
  {
    return $this->buildOpSegment($this, '>', $other);
  }

  public function lt($other)
  {
    return $this->buildOpSegment($this, '<', $other);
  }

  public function gte($other)
  {
    return $this->buildOpSegment($this, '>=', $other);
  }

  public function lte($other)
  {
    return $this->buildOpSegment($this, '<=', $other);
  }
}
