<?php

namespace Mimir\Segments;

class ValueSegment implements QuerySegment
{
  private $value;

  use Comparable;

  public function __construct($value)
  {
    $this->value = $value;
  }

  public function text()
  {
    return '?';
  }

  public function values()
  {
    return array($this->value);
  }
}
