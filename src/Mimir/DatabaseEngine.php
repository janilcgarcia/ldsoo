<?php

namespace Mimir;

class DatabaseEngine
{
  private $pdo;

  public static function __dependencies()
  {
    return ['dbConfig'];
  }

  public function setDbConfig($config)
  {
    $driver = $config['driver'];
    undef($config['driver']);
    $dsn = array();

    foreach ($config as $key => $value) {
      $dsn[] = "$key=$value";
    }

    $dsn = 'pgsql:' . implode(';', $dsn);

    $this->pdo = new PDO($dsn);
  }

  public function select($fields)
  {
    if (isarray($fields))
      return new SelectStatement($fields);

    $meta = new MetaModel($fields);

    return (new SelectStatement($this->pdo, $meta->getFields()))
        ->from($meta->getTable())->map(function ($data) {
          $class = new \ReflectionClass($fields);
          $obj = $class->newInstance();

          foreach ($data as $key => $value) {
            $m = $class->getMethod('set' . ucfirst($key));
            $m->invoke($obj, $value);
          }

          return $obj;
        });
  }

  public function insert($model)
  {
    $meta = new MetaModel($model);
    return new InsertStatement($meta);
  }
}
