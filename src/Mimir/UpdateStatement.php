<?php

namespace Mimir;

class UpdateStatement
{
  private $meta;
  private $value;

  public function __construct($pdo, $meta)
  {
    $this->meta = $meta;
  }

  public function value($value)
  {
    $this->value = $value;
    return $this;
  }

  public function makeQuery()
  {
    $fields = $this->meta->fields();
    \array_splice($fields, \array_search($this->meta->id(), $fields), 1);

    $sf = \Mimir\Segments\SegmentFactory::getInstance();
    $query = $sf->composite('',
      'UPDATE ',
      $sf->id($this->meta->table()),
      ' SET '
    );
    $g = $sf->group(false);
    $query->add($g);

    foreach ($fields as $field) {
      $g->add($sf->id($field)->eq($this->value->get($field)));
    }

    $query->add($sf->plain(' WHERE '));
    $query->add($sf->id($this->meta->id())->eq($this->value->get($this->meta->id())));

    return $query;
  }

  public function execute()
  {
    $q = $this->makeQuery();

    $s = $this->pdo->prepare($q->text());

    $this->pdo->beginTransaction();

    try {
      $s->execute($q->values());

      $this->pdo->commit();
    } catch (\Exception $ex) {
      $this->pdo->rollback();

      throw $ex;
    }
  }
}
