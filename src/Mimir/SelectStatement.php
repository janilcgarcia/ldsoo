<?php

namespace Mimir;

class SelectStatement
{
  private $conn;
  private $meta;

  private $fields;
  private $table;

  private $where;
  private $order;

  private $offset;
  private $limit;

  public function __construct($conn, $meta)
  {
    $this->conn = $conn;
    $this->meta = $meta;

    $this->fields = $this->meta->fields();
    $this->table = $this->meta->table();

    $this->where = null;
    $this->order = null;

    $this->offset = null;
    $this->limit = null;
  }

  public function where($expr)
  {
    $this->where = $expr;
    return $this;
  }

  public function order($fields)
  {
    $this->order = new Segments\GroupSegment();

    foreach ($fields as $field) {
      if (strpos('-', $field) === 0) {
        $order = new Segments\CompositeSegment(' ');
        $order->add(new Segments\IdSegment(substr($field, 1)));
        $order->add(new Segments\PlainSegment('DESC'));

        $this->order->add($order);
      } else {
        $this->order->add(new Segments\IdSegment($field));
      }
    }

    return $this;
  }

  public function limit($limit, $offset = null)
  {
    $this->limit = $limit;
    $this->offset = $offset;
    return $this;
  }

  public function makeQuery()
  {
    $sf = Segments\SegmentFactory::getInstance();

    $query = $sf->composite('', $sf->plain('SELECT '),
        $sf->group(false, ...array_map(array($sf, 'id'), $this->fields)),
        $sf->plain(' FROM '),
        $sf->id($this->table));

    if ($this->where) {
      $query->add($sf->plain(' WHERE '));
      $query->add($this->where);
    }

    if ($this->order) {
      $query->add($sf->plain(' ORDER BY '));
      $query->add($this->order);
    }

    if ($this->limit) {
      $query->add($sf->plain(' LIMIT '));
      $query->add($sf->plain($this->limit));

      if ($this->offset) {
        $query->add($sf->plain(' OFFSET '));
        $query->add($sf->plain($this->offset));
      }
    }

    return $query;
  }

  public function all()
  {
    $q = $this->makeQuery();
    $stmt = $this->pdo->prepare($q->text());

    $stmt->execute($q->values());

    $list = $stmt->fetchAll(PDO::FETCH_ASSOC);

    return array_map(array($this->meta, 'make'), $list);
  }

  public function first()
  {
    $limit = $this->limit;
    $this->limit(1, $this->offset);

    $q = $this->makeQuery();

    $stmt = $this->pdo->prepare($q->text());
    $stmt->execute($q->values());

    $this->limit($limit, $this->offset);

    $selected = $stmt->fetch(PDO::FETCH_ASSOC);

    if (!$selected)
      return null;

    return $this->meta->make($selected);
  }
}
