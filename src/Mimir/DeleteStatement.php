<?php

namespace Mimir;

class DeleteStatement
{
  private $pdo;
  private $meta;
  private $where;

  public function __construct($pdo, $meta)
  {
    $this->pdo = $pdo;
    $this->meta = $meta;
    $this->where = null;
  }

  public function where($where)
  {
    $this->where = $where;
    return $this;
  }

  public function makeQuery()
  {
    $sf = Segments\SegmentFactory::getInstance();
    $s = $sf->composite('',
      'DELETE FROM ',
      $sf->id($this->meta->table())
    );

    if ($this->where) {
      $s->add($sf->plain(' WHERE '));
      $s->add($this->where);
    }

    return $s;
  }

  public function execute()
  {
    $q = $this->makeQuery();

    $s = $this->pdo->prepare($q->text());

    $this->pdo->beginTransaction();

    try {
      $s->execute($q->values());

      $this->pdo->commit();
    } catch (\Exception $ex) {
      $this->pdo->rollback();

      throw $ex;
    }
  }
}
