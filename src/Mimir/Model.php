<?php

namespace Mimir;

class Model
{
  const table = '?';
  const id = 'id';
  const fields = array();

  private $dict;
  private $meta;

  public function __construct($dict = array())
  {
    $this->dict = $dict;
    $this->meta = new MetaModel(get_class($this));

    foreach ($this->meta->fields() as $f) {
      if (!isset($this->dict[$f]))
        $this->dict[$f] = null;
    }
  }

  public function set($name, $value)
  {
    if (in_array($name, $this->meta->fields()))
      $this->dict[$name] = $value;
    else
      throw new \Exception("$name: Field not found in model");
  }

  public function get($name) {
    if (!isset($this->dict))
      throw new \Exception("$name: Field not found in model");

    return $this->dict[$name];
  }
}
