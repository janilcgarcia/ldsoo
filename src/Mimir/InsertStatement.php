<?php

namespace Mimir;

class InsertStatement
{
  private $meta;
  private $values;
  private $segment;

  public function __construct($pdo, $meta)
  {
    $this->meta = $meta;
  }

  public function values(...$values)
  {
    $this->values = $values;
    return $this;
  }

  public function makeQuery()
  {
    $fields = $this->meta->fields();
    \array_splice($fields, \array_search($this->meta->id(), $fields), 1);

    $sf = \Mimir\Segments\SegmentFactory::getInstance();
    $query = $sf->composite('',
      'INSERT INTO ',
      $sf->id($this->meta->table()),
      $sf->group(true, ...array_map(array($sf, 'id'), $fields)),
      ' VALUES ');

    $values = $sf->group(false);
    $query->add($values);

    foreach ($this->values as $value) {
      $g = $sf->group(true);
      $values->add($g);

      foreach ($fields as $field)
        $g->add($sf->value($value->get($field)));
    }

    return $query;
  }

  public function execute()
  {
    $q = $this->makeQuery();

    $s = $this->pdo->prepare($q->text());

    $this->pdo->beginTransaction();

    try {
      $s->execute($q->values());

      $this->values[count($this->values) - 1]->set($this->meta->id(),
          $this->pdo->lastInsertedId());

      $this->pdo->commit();
    } catch (\Exception $ex) {
      $this->pdo->rollback();

      $this->values[count($this->values) - 1]->set('id', null);

      throw $ex;
    }
  }
}
