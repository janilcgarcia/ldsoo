<?php

namespace Mimir;

class MetaModel
{
  private $class;

  public function __construct($model)
  {
    $this->class = new \ReflectionClass($model);
  }

  public function id()
  {
    return $this->class->getConstant('id');
  }

  public function fields()
  {
    return $this->class->getConstant('fields');
  }

  public function table()
  {
    return $this->class->getConstant('table');
  }

  public function make($values)
  {
    return $this->class->newInstance($values);
  }
}
