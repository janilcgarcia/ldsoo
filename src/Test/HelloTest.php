<?php

namespace Test;

use testing\TestCase;

class HelloTest extends TestCase
{
    public function testHello()
    {
        $this->assertEqual(2, 2);
        $this->ok();
        $this->fail('Super failed');
        $this->ok();
        $this->ok();
        $this->fail('Other fail');

        $this->assert(false, 'YAF');

        $this->assertThrows(['Exception', 'RuntimeException'], function () {
            throw new \RuntimeException('Throwing the right exception');
        });
    }
}
