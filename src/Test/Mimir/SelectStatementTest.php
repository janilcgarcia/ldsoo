<?php

namespace Test\Mimir;

use \Mimir\SelectStatement;
use \testing\TestCase;

class SelectStatementTest extends TestCase
{
  public function testBasicQuery()
  {
    $s = new SelectStatement(null, new \Mimir\MetaModel(MimirModel::class));

    $this->assertEqual($s->makeQuery()->text(), 'SELECT "id", "name", "email", "hash", "age" FROM "mimir"');
  }

  public function testSingleWhere()
  {
    $sql = '' .
    'SELECT "id", "name", "email", "hash", "age" FROM "mimir" WHERE "age" > ?' .
    '';

    $sf = \Mimir\Segments\SegmentFactory::getInstance();
    $s = new SelectStatement(null, new \Mimir\MetaModel(MimirModel::class));
    $s->where($sf->id('age')->gt(10));

    $q = $s->makeQuery();

    $this->assertEqual($q->text(), $sql);
    $this->assertEqual($q->values(), array(10));
  }

  public function testTwoWhere()
  {
    $sql =
    'SELECT "id", "name", "email", "hash", "age" FROM "mimir" WHERE "age" > ? AND "name" = ?';

    $sf = \Mimir\Segments\SegmentFactory::getInstance();
    $s = new SelectStatement(null, new \Mimir\MetaModel(MimirModel::class));

    $s->where($sf->and($sf->id('age')->gt(18), $sf->id('name')->eq('peter')));

    $q = $s->makeQuery();

    $this->assertEqual($q->text(), $sql);
    $this->assertEqual($q->values(), array(18, 'peter'));
  }

  public function testThreeWhere()
  {
    $sql =
    'SELECT "id", "name", "email", "hash", "age" FROM "mimir" WHERE ("age" > ? AND "name" = ?) AND "id" < ?';

    $sf = \Mimir\Segments\SegmentFactory::getInstance();
    $s = new SelectStatement(null, new \Mimir\MetaModel(MimirModel::class));

    $s->where($sf->and(
      $sf->and($sf->id('age')->gt(18), $sf->id('name')->eq('peter')),
      $sf->id('id')->lt(20)
    ));

    $q = $s->makeQuery();

    $this->assertEqual($q->text(), $sql);
    $this->assertEqual($q->values(), array(18, 'peter', 20));
  }
}
