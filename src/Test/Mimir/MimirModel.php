<?php

namespace Test\Mimir;

use \Mimir\Model;

class MimirModel extends Model
{
  const id = 'id';
  const fields = array('id', 'name', 'email', 'hash', 'age');
  const table = 'mimir';
}
