<?php

namespace Test\Mimir;

use \Mimir\UpdateStatement;

class UpdateStatementTest extends \testing\TestCase
{
  public function testSingleUpdate()
  {
    $s = new UpdateStatement(null, new \Mimir\MetaModel(MimirModel::class));

    $value = new MimirModel(array(
      'id' => 14,
      'name' => 'mimir',
      'email' => 'mimir@asgard.gov',
      'hash' => 'hashy',
      'age' => 8299
    ));

    $sql = 'UPDATE "mimir" SET "name" = ?, "email" = ?, "hash" = ?, "age" = ? WHERE "id" = ?';

    $s->value($value);
    $q = $s->makeQuery();

    $this->assertEqual($sql, $q->text());
    $this->assertEqual(array('mimir', 'mimir@asgard.gov', 'hashy', 8299, 14), $q->values());
  }
}
