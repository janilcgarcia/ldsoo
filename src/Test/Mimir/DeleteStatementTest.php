<?php

namespace Test\Mimir;

class DeleteStatementTest extends \testing\TestCase
{
  public function testDelete()
  {
    $sf = \Mimir\Segments\SegmentFactory::getInstance();

    $s = new \Mimir\DeleteStatement(null, new \Mimir\MetaModel(MimirModel::class));
    $s->where($sf->id('id')->eq(10));
    $q = $s->makeQuery();

    $this->assertEqual($q->text(), 'DELETE FROM "mimir" WHERE "id" = ?');
    $this->assertEqual($q->values(), array(10));
  }
}
