<?php

namespace Test\Mimir;

use \Mimir\InsertStatement;

class InsertStatementTest extends \testing\TestCase
{
  public function testSingleInsert()
  {
    $s = new InsertStatement(null, new \Mimir\MetaModel(MimirModel::class));

    $values = [new MimirModel(array(
      'name' => 'mimir',
      'email' => 'mimir@asgard.gov',
      'hash' => 'hashy',
      'age' => 8299
    ))];

    $sql = 'INSERT INTO "mimir"("name", "email", "hash", "age") VALUES (?, ?, ?, ?)';

    $s->values(...$values);
    $q = $s->makeQuery();

    $this->assertEqual($sql, $q->text());
    $this->assertEqual(array('mimir', 'mimir@asgard.gov', 'hashy', 8299), $q->values());
  }

  public function testDualInsert()
  {
    $s = new InsertStatement(null, new \Mimir\MetaModel(MimirModel::class));

    $values = [new MimirModel(array(
      'name' => 'mimir',
      'email' => 'mimir@asgard.gov',
      'hash' => 'hashy',
      'age' => 8299
    )), new MimirModel(array(
      'name' => 'thor',
      'email' => 'thor@asgard.gov',
      'hash' => 'hashy',
      'age' => 9001
    ))];

    $qv = array();

    foreach ($values as $v) {
      foreach (MimirModel::fields as $field) {
        if ($field !== MimirModel::id)
          $qv[] = $v->get($field);
      }
    }

    $sql = 'INSERT INTO "mimir"("name", "email", "hash", "age") VALUES (?, ?, ?, ?), (?, ?, ?, ?)';

    $s->values(...$values);
    $q = $s->makeQuery();

    $this->assertEqual($sql, $q->text());
    $this->assertEqual($qv, $q->values());
  }
}
