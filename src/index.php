<?php

require_once './autoload.php';

use locator\ComponentLocator;

$locator = new ComponentLocator;
$locator->register('dispatcher',
                   locator\instance('http\\RequestDispatcher'));

$locator->register('router',
                   locator\instance('http\\Router'));

$locator->register('bodyParsers', function ($params) {
    $bp = new http\BodyParsers;

    $bp->register(['application/x-www-form-urlencoded',
                   new http\FormBodyParser]);
    $bp->register(['application/json', new http\JsonBodyParser]);

    return $bp;
});

$locator->register('controller/home', locator\instance('Controllers\HomeController'));
$locator->register('controller/login', locator\instance('Controllers\LoginController'));
$locator->register('templates', function ($p) {
  $e = new \Modus\Engine(dirname(__FILE__) . '/views/', array());

  return $e;
});

$router = $locator->find('router');

$router->register('/', 'home#index');
$router->register('/login', 'login#index');

$locator->find('dispatcher')->dispatch();
