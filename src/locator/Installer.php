<?php

namespace locator;

interface Installer
{
    public function install($obj);
}
