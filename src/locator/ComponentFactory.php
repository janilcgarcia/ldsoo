<?php

namespace locator;

class ComponentFactory
{
    private $id_match;
    private $callback;
    
    public function __construct($id_match, $callback)
    {
        $this->callback = $callback;

        $id_match = \preg_quote($id_match, '!');
        $id_match = \preg_replace('!\\\\{(.+?)\\\\}!',
                                  '(?P<$1>[^/]+)', $id_match);

        $this->id_match = '!^' . $id_match . '$!';
    }

    public function match($id)
    {
        if (\preg_match($this->id_match, $id, $matches)) {
            return $matches;
        }
    }

    public function build($locator, $params) {
        return \call_user_func($this->callback, $params);
    }
}
