<?php

namespace locator;

class PropertyInstaller implements Installer
{
    private $object;
    private $property;

    public function __construct($obj, $name)
    {
        $this->object = $obj;
        $obj = new \ReflectionObject($obj);

        $this->property = $obj->getProperty($name);
    }

    public function install($obj)
    {
        $this->property->setAccessible(true);
        $this->property->setValue($this->object, $obj);
        $this->property->setAccessible(false);
    }
}
