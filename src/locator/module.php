<?php

namespace locator;

function value($val)
{
    return function ($params) use ($val) {
        return $val;
    };
}

function instance($name)
{
    return function ($params) use ($name) {
        $c = new \ReflectionClass($name);

        return $c->newInstance();
    };
}

