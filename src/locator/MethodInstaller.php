<?php

namespace locator;

class MethodInstaller implements Installer
{
    private $object;
    private $method;

    public function __construct($obj, $method)
    {
        $this->object = $obj;

        $obj = new \ReflectionObject($obj);
        $this->method = $obj->getMethod($method);
    }

    public function install($obj)
    {
        $this->method->invoke($this->object, $obj);
    }
}
