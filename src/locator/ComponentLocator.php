<?php

namespace locator;

class ComponentLocator
{
    private $factories;
    private $cache;

    public function __construct()
    {
        $this->factories = array();
        $this->components = array('locator' => $this);
    }

    public function register($id, $factory)
    {
        $this->factories[] = new ComponentFactory($id, $factory);
    }

    public function find($id)
    {
        if (\array_key_exists($id, $this->components))
            return $this->components[$id];

        foreach ($this->factories as $factory) {
            if ($params = $factory->match($id)) {
                $this->components[$id]
                    = $factory->build($this, $params);

                if (is_object($this->components[$id])) {
                    $c = new \ReflectionClass($this->components[$id]);

                    if ($c->hasMethod('__dependencies')
                        && (($method = $c->getMethod('__dependencies'))
                            ->isStatic())) {
                    
                        $deps = $method->invoke($this->components[$id]);
                        
                        $this->installDependencies($this->components[$id],
                                                   $deps);
                    }
                }
                
                return $this->components[$id];
            }
        }

        throw new LocatorException("Can't find object {$id}");
    }

    private function installDependencies($obj, $deps)
    {
        foreach ($deps as $key => $value)
        {
            $located = $this->find($value);
            
            if (is_numeric($key))
                $inst = new SetterInstaller($obj, $value);
            else if (preg_match('/^p:[^:]/', $key))
                $inst = new PropertyInstaller($obj, substr($key, 2));
            else if (preg_match('/^m:[^:]/', $key))
                $inst = new MethodInstaller($obj, substr($key, 2));
            else
                $inst = new SetterInstaller($obj, $key);

            $inst->install($located);
        }
    }
}
