<?php

namespace locator;

class SetterInstaller extends MethodInstaller
{
    public function __construct($obj, $property)
    {
        parent::__construct($obj, 'set' . ucfirst($property));
    }
}

