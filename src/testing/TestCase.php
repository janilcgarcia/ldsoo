<?php

namespace testing;

abstract class TestCase
{
    private $runner;

    public function __construct($runner)
    {
        $this->runner = $runner;
    }

    public function setup()
    {
    }

    public function teardown()
    {
    }

    public function setupCase()
    {
    }

    public function teardownCase()
    {
    }

    public function ok()
    {
        $this->assert(true);
    }

    public function fail($message = null)
    {
        $this->assert(false, $message);
    }

    public function assert($value, $message = null)
    {
        if ($value)
        {
            $this->runner->register(true);
            return true;
        }
        else
        {
            if (!$message) $message = '';
            $this->runner->register(false, $message);

            return false;
        }
    }

    public function assertEqual($a, $b, $message = null)
    {
        $msg = 'Expected ' . serialize($a) . ' === ' . serialize($b);
        if ($message) $msg .= ': ' . $message;

        $this->assert($a === $b, $msg);
    }

    public function assertNotEqual($a, $b, $message = null)
    {
        $msg = "Expected $a !== $b";
        if ($message) $msg .= ': ' . $message;

        $this->assert($a !== $b, $message);
    }

    public function assertLessThan($a, $b, $message = null)
    {
        $msg = "Expected $a < $b";
        if ($message) $msg .= ': ' . $message;

        $this->assert($a < $b, $message);
    }

    public function assertMoreThan($a, $b, $message = null)
    {
        $msg = "Expected $a > $b";
        if ($message) $msg .= ': ' . $message;

        $this->assert($a > $b, $message);
    }

    public function assertLessEqual($a, $b, $message = null)
    {
        $msg = "Expected $a <= $b";
        if ($message) $msg .= ': ' . $message;

        $this->assert($a <= $b, $message);
    }

    public function assertMoreEqual($a, $b, $message = null)
    {
        $msg = "Expected $a >= $b";
        if ($message) $msg .= ': ' . $message;

        $this->assert($a >= $b, $message);
    }

    public function assertTrue($value, $message = null)
    {
        $this->assert($value, $message);
    }

    public function assertFalse($value, $message = null)
    {
        $this->assert($value, $message);
    }

    public function assertNull($value, $message = null)
    {
        $this->assert($value === null, $message);
    }

    public function assertNotNull($value, $message = null)
    {
        $this->assert($value !== null, $message);
    }

    public function assertIn($needle, $haystack, $message = null)
    {
        $hay = '[' . implode(',', $haystack) . ']';
        $msg = "Expected $needle to be in $hay";

        if ($message)
            $msg .= ": $message";

        return $this->assert(in_array($needle, $haystack), $msg);
    }

    public function assertThrows($exceptions, $callback)
    {
        try
        {
            $callback();
            $this->fail('Code ran without exceptions');
        }
        catch (\Exception $e)
        {
            if (!is_array($exceptions))
                $exceptions = array($exceptions);

            if ($this->assertIn(get_class($e), $exceptions))
                return $e;
        }

        return FALSE;
    }
}
