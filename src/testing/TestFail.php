<?php

namespace testing;

class TestFail
{
    private $message;
    private $file;
    private $line;

    public function __construct($message, $file, $line)
    {
        $this->message = $message;
        $this->file = $file;
        $this->line = $line;

        if (!$this->message)
        {
            $this->message = 'Assert Failed';
        }
    }

    public function print()
    {
        echo "{$this->file}:{$this->line}: {$this->message}\n";
    }
}
