<?php

Test\describe('A Stack', function () {
    Test\describe('#pop', function () {
        Test\it('should throw an exception if empty',
                Test\throws(['StackEmptyException'], function () {
                    $stack = new Stack;
                    $stack->pop();
                })
        );
    });
});
