<?php

namespace testing;

class TestRunner
{
    private $failed;
    private $case;

    public function __construct()
    {
        $this->case = null;
        $this->failed = [];
    }

    public function register($success, $message = null)
    {
        if ($success) {
            echo '.';
        } else {
            echo 'F';

            $backtrace = debug_backtrace();
            $bt_id = 0;

            while ($bt_id < count($backtrace) &&
                   $backtrace[$bt_id]['class'] !== $this->case)
            {
                $bt_id += 1;
            }

            $bt = $backtrace[$bt_id - 1];

            $this->failed[] = new TestFail($message, $bt['file'], $bt['line']);
        }
    }

    public function run($directory)
    {
        $files = array_filter(scandir($directory), function ($e) {
            return $e[0] !== '.';
        });

        $files = array_values($files);

        foreach ($files as &$f)
        {
            $f = $directory . '/' . $f;
        }

        $cases = array();

        foreach ($files as $file)
        {
            if (is_file($file))
            {
                //include $file;

                $className = str_replace('/', '\\',
                                         substr($file, 0,
                                                strrpos($file, '.php')));

                $cls = new \ReflectionClass($className);

                if ($cls->isSubclassOf(TestCase::class)) {
                  $obj = $cls->newInstance($this);

                  $this->runCase($cls, $obj);
                }
            }
            else if (is_dir($file))
            {
                $this->run($file);
            }
        }
    }

    private function runCase($cls, $obj)
    {
        $this->startCase($cls, $obj);
        $methods = $cls->getMethods();

        foreach ($methods as $method) {
            if (substr($method->getName(), 0, 4) === 'test')
            {
                $this->startTest($method->getName(), $obj);

                try
                {
                    $method->invoke($obj);
                }
                catch (\Exception $e)
                {
                    echo 'E';
                    $this->failed[] = new TestFail('Exception thrown: ' . $e->getMessage(),
                                                   $e->getFile(), $e->getLine());
                }

                echo "\n";
                $this->endTest($obj);
                echo "\n";
            }
        }
        $this->endCase($obj);
    }

    private function startCase($cls, $obj)
    {
        $obj->setupCase();
        $this->case = $cls->getName();

        echo $cls->getName() . "\n";
    }

    private function endCase($obj)
    {
        $obj->teardownCase();
        $this->case = null;
    }

    private function startTest($name, $obj)
    {
        $obj->setup();
        echo '  ->' . $name . ': ';
    }

    private function endTest($obj)
    {
        $obj->teardown();

        foreach ($this->failed as $fail)
        {
            echo '    ';
            $fail->print();
            echo "\n";
        }


        $this->failed = [];
    }
}
