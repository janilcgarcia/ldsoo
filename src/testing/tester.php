<?php

namespace testing;


require dirname(__DIR__) . '/' . 'autoload.php';

if (count($argv) < 2)
{
    echo "Usage: php {$argv[0]} [pkg]\n";
    exit(1);
}

$pkg = $argv[1];

$runner = new TestRunner;

$runner->run($pkg);


