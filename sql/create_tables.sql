CREATE TABLE "users"(
  "id" SERIAL PRIMARY KEY,
  "name" VARCHAR(256) NOT NULL,
  "email" VARCHAR(256) NOT NULL,
  "hash" VARCHAR(128) NOT NULL,
  "admin" BOOLEAN NOT NULL DEFAULT 'true'
);

CREATE TABLE "sectors"(
  "id" SERIAL PRIMARY KEY,
  "name" VARCHAR(256) NOT NULL
);

CREATE TABLE "activities"(
  "id" SERIAL PRIMARY KEY,
  "name" VARCHAR(256) NOT NULL,
  "description" VARCHAR(2048),
  "sector_id" INTEGER NOT NULL,

  CONSTRAINT "activity_sector_fk" FOREIGN KEY ("sector_id") REFERENCES "sectors"("id")
);

CREATE TABLE "event_types"(
  "id" SERIAL PRIMARY KEY,
  "name" VARCHAR(256) NOT NULL
);

CREATE TABLE "events"(
  "id" SERIAL PRIMARY KEY,
  "name" VARCHAR(256) NOT NULL,
  "event_type_id" INTEGER NOT NULL,
  "date" DATE NOT NULL,

  CONSTRAINT "event_event_type_fk" FOREIGN KEY ("event_type_id") REFERENCES "event_types"("id")
);

CREATE TABLE "volunteer_schedule"(
  "id" SERIAL PRIMARY KEY,
  "event_id" INTEGER NOT NULL,
  "volunteer_id" INTEGER NOT NULL,
  "activity_id" INTEGER NOT NULL,
  "status" INTEGER NOT NULL DEFAULT 0,

  CONSTRAINT "volunteer_schedule_volunteer_fk"
    FOREIGN KEY ("volunteer_id") REFERENCES "users"("id"),
  CONSTRAINT "volunteer_schedule_activity_fk"
    FOREIGN KEY ("activity_id") REFERENCES "activities"("id"),
  CONSTRAINT "volunteer_schedule_event_fk"
    FOREIGN KEY ("event_id") REFERENCES "events"("id")
);

CREATE TABLE "volunteer_sector"(
  "volunteer_id" INTEGER,
  "sector_id" INTEGER,

  CONSTRAINT "volunteer_sector_user_fk"
    FOREIGN KEY ("volunteer_id") REFERENCES "users"("id"),
  CONSTRAINT "volunteer_sector_sector_fk"
    FOREIGN KEY ("sector_id") REFERENCES "sectors"("id")
)
