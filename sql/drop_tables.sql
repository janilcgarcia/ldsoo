DROP TABLE IF EXISTS "users";
DROP TABLE IF EXISTS "sectors";
DROP TABLE IF EXISTS "activities";
DROP TABLE IF EXISTS "event_types";
DROP TABLE IF EXISTS "events";
DROP TABLE IF EXISTS "volunteer_schedule";
DROP TABLE IF EXISTS "volunteer_sector";
